class Grid {
  private currentGeneration: number[][];

  public constructor(
    private readonly columnCount: number,
    private readonly rowCount: number,
    private readonly generationZeroSeed: string[]
  ) {
    this.currentGeneration = this.generationZeroSeed.map((row: string) =>
      row.split('').map(Number)
    );
  }

  public nextGeneration(): void {
    const newGeneration: number[][] = [];

    const startRow: number = 0;
    const endRow: number = this.rowCount - 1;
    const startCol: number = 0;
    const endCol: number = this.columnCount - 1;

    for (let i: number = startRow; i <= endRow; i++) {
      newGeneration.push([]);

      for (let j: number = startCol; j <= endCol; j++) {
        let greenNeighbours: number = 0;

        // Counting all neighbouring green cells
        if (i > startRow && this.currentGeneration[i - 1][j] === 1) {
          greenNeighbours++;
        }
        if (j > startCol && this.currentGeneration[i][j - 1] === 1) {
          greenNeighbours++;
        }
        if (i > startRow && j > startCol && this.currentGeneration[i - 1][j - 1] === 1) {
          greenNeighbours++;
        }
        if (i < endRow && this.currentGeneration[i + 1][j] === 1) {
          greenNeighbours++;
        }
        if (j < endCol && this.currentGeneration[i][j + 1] === 1) {
          greenNeighbours++;
        }
        if (i < endRow && j < endCol && this.currentGeneration[i + 1][j + 1] === 1) {
          greenNeighbours++;
        }
        if (i < endRow && j > startCol && this.currentGeneration[i + 1][j - 1] === 1) {
          greenNeighbours++;
        }
        if (i > startRow && j < endCol && this.currentGeneration[i - 1][j + 1] === 1) {
          greenNeighbours++;
        }

        // Updating the cell based on the task requirements
        if (this.isCellGreen(j, i)) {
          greenNeighbours === 2 || greenNeighbours === 3 || greenNeighbours === 6
            ? newGeneration[i].push(1)
            : newGeneration[i].push(0);
        } else {
          greenNeighbours === 3 || greenNeighbours === 6
            ? newGeneration[i].push(1)
            : newGeneration[i].push(0);
        }
      }
    }
    this.currentGeneration = newGeneration;
  }

  public isCellGreen(x: number, y: number): boolean {
    return this.currentGeneration[y][x] === 1 ? true : false;
  }
}

class GridGame {
  public constructor(private readonly grid: Grid) {}

  public play(targetCellX: number, targetCellY: number, finalGeneration: number): number {
    let greenCounter: number = this.grid.isCellGreen(targetCellX, targetCellY) ? 1 : 0;

    for (let i = 0; i < finalGeneration; i++) {
      this.grid.nextGeneration();

      if (this.grid.isCellGreen(targetCellX, targetCellY)) {
        greenCounter++;
      }
    }

    return greenCounter;
  }
}

// Testing Area
const gridColumnCount: number = 4;
const gridRowCount: number = 4;
const generationZeroSeed: string[] = ['1001', '1111', '0100', '1010'];

const targetCellX: number = 2;
const targetCellY: number = 2;
const finalGeneration: number = 15;

const grid: Grid = new Grid(gridColumnCount, gridRowCount, generationZeroSeed);
const gridGame: GridGame = new GridGame(grid);

const result: number = gridGame.play(targetCellX, targetCellY, finalGeneration);

console.log(result);
