#### Instructions to run the program:

1 Download and install Git: https://git-scm.com/downloads

1.1 Copy the project URL form GitLab.

<img src="./screenshots/gitlab-clone.png" width="350px" />

1.2 Choose a place for the project folder, then right click and Git Bash Here.

<img src="./screenshots/git-bash-here.png" width="125px" />

1.3 Type git clone, then right click and paste the URL, then press enter.

<img src="./screenshots/git-clone.png" width="300px" />

2 Download and install Visual Studio Code: https://code.visualstudio.com/

3 Download and install Node.js: https://nodejs.org/en/download/

4 Open the project folder in Visual Studio Code.

<img src="./screenshots/open-folder.png" width="150px" />

4.1 Open a terminal.

<img src="./screenshots/new-terminal.png" width="150px" />

4.2 Install TypeScript: Type **npm install -g typescript** in the terminal.

4.3 Install TS-Node: Type **npm install -g ts-node** in the terminal.

5 Type **ts-node task-solution.ts** in the terminal to execute the program.

The **testing area** at the bottom of the task-solution.ts file can be used to change the parameters.
